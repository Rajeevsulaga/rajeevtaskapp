package com.test.testapp.util

import androidx.room.TypeConverter
import androidx.room.TypeConverters

/**
 * Created by rajeev on 22,August,2020
 */
object Constants {
    const val VIEW_NOT_ATTACHED_MSG =
        "Please call Presenter.attachView(MvpView) before requesting data to the Presenter"
}