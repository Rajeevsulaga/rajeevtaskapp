package com.test.testapp.database

/**
 * Created by rajeev on 22,August,2020
 */
const val DB_NAME = "employee_db"
object ENTITIES {
    const val EMPLOYEE = "employee"
    const val CATEGORY = "category"
    const val ROLE = "role"
}