package com.test.testapp.presenter

import androidx.lifecycle.LiveData
import com.test.testapp.database.entities.Category
import com.test.testapp.database.entities.Employee
import com.test.testapp.database.entities.Role
import com.test.testapp.repository.EmployeeRepository
import com.test.testapp.view.IEmployeeFormView

/**
 * Created by rajeev on 22,August,2020
 */
class EmployeeFormPresenter : BasePresenter<IEmployeeFormView>() {

    private val repository = EmployeeRepository.INSTANCE

    fun createEmployeeRecord(employee: Employee) {
       repository.createEmployee(employee)
    }

    fun getEmployeeData(id: Int): LiveData<Employee?> {
        return repository.getEmployeeForId(id)
    }

    fun getRoles() : LiveData<List<Role>> {
        return repository.getEmployeeRoles()
    }

    fun getCategories() : LiveData<List<Category>> {
        return repository.getEmployeeCategories()
    }
}