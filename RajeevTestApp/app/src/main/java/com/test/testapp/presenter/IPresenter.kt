package com.test.testapp.presenter

import com.test.testapp.view.IBaseView


/**
 * Created by rajeev on 22,August,2020
 */
interface IPresenter<V : IBaseView> {
    fun attachView(view: V)

    fun detachView()
}