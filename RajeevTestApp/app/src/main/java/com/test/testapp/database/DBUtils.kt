package com.test.testapp.database

import android.content.Context
import androidx.lifecycle.LiveData
import com.test.testapp.database.entities.Category
import com.test.testapp.database.entities.Employee
import com.test.testapp.database.entities.Role
import kotlinx.coroutines.runBlocking
import java.util.*

/**
 * Created by rajeev on 22,August,2020
 */
object DBUtils {
    private lateinit var mDatabase: AppDatabase
    fun onCreate(context: Context) {
        mDatabase = AppDatabase.getDatabase(context)
        mDatabase.employeeDao().getEmployeeForId(-1)
    }

    fun getDatabase() : AppDatabase {
        return mDatabase
    }

    fun getAllEmployees(): LiveData<List<Employee>> {
        return mDatabase.employeeDao().getAllEmployees()
    }

    fun getEmployeeCategories(): LiveData<List<Category>> {
        return mDatabase.employeeDao().getAllCategories()
    }

    fun getEmployeeData(id: Int): LiveData<Employee?> {
        return mDatabase.employeeDao().getEmployeeForId(id)
    }

    fun getEmployeeRoles(): LiveData<List<Role>> {
        return mDatabase.employeeDao().getAllRoles()
    }

    suspend fun deleteEmployeeRecord(id: Int) {
        mDatabase.employeeDao().deleteEmployee(id)
    }

    suspend fun createEmployee(employee: Employee) {
        mDatabase.employeeDao().createEmployee(employee)
    }

    fun getCategories(): List<Category> {
        return listOf(
            Category(type = "QA"),
            Category(type = "Developer"),
            Category(type = "Manager"),
            Category(type = "Security Analyst")
        );
    }

    fun getRoles(): List<Role> {
        return listOf(
            Role(type = "Admin"),
            Role(type = "Moderator"),
            Role(type = "HR"),
            Role(type = "Lead")
        );
    }


}