package com.test.testapp.presenter

import com.test.testapp.view.IBaseView


/**
 * Created by rajeev on 22,August,2020
 */
open class BasePresenter<T : IBaseView> : IPresenter<T> {
    private var mView: T? = null

    override fun attachView(mvpView: T) {
        mView = mvpView
    }

    override fun detachView() {
        mView = null
    }

    fun isViewAttached(): Boolean {
        return mView != null
    }

    fun getMvpView(): T? {
        return mView
    }

    fun checkViewAttached() {
        if (!isViewAttached()) throw ViewNotAttachedException()
    }

    open class ViewNotAttachedException : RuntimeException()
}