package com.test.testapp.database

import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.test.testapp.database.dao.EmployeeDao
import com.test.testapp.database.entities.Category
import com.test.testapp.database.entities.Employee
import com.test.testapp.database.entities.Role
import kotlinx.coroutines.runBlocking

/**
 * Created by rajeev on 22,August,2020
 */

const val APP_DB_VERSION = 1

@Database(
    entities = [Employee::class, Category::class, Role::class],
    version = APP_DB_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    private lateinit var dbThread: HandlerThread
    lateinit var dbThreadHandler : Handler
    abstract fun employeeDao(): EmployeeDao

    companion object {
        @Volatile
        private var appInstance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if (null !== appInstance) {
                return appInstance as AppDatabase
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DB_NAME
                ).addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        appInstance?.dbThreadHandler?.post {
                            runBlocking {
                                appInstance?.employeeDao()?.createCategories(DBUtils.getCategories())
                                appInstance?.employeeDao()?.createRoles(DBUtils.getRoles())
                            }
                        }
                    }

                    override fun onOpen(db: SupportSQLiteDatabase) {
                        super.onOpen(db)
                    }

                }).build()
                appInstance = instance
                appInstance?.dbThread = HandlerThread("DB")
                appInstance?.dbThread?.start()

                appInstance?.dbThreadHandler = Handler(appInstance?.dbThread?.looper)
                return instance
            }
        }

        fun getDBVersion(): Int {
            return APP_DB_VERSION
        }
    }
}