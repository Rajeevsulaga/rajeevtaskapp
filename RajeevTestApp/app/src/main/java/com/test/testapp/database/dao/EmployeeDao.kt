package com.test.testapp.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.test.testapp.database.ENTITIES
import com.test.testapp.database.entities.Category
import com.test.testapp.database.entities.Employee
import com.test.testapp.database.entities.Role

/**
 * Created by rajeev on 22,August,2020
 */
@Dao
interface EmployeeDao {
    @Query("SELECT * from ${ENTITIES.EMPLOYEE}")
    fun getAllEmployees(): LiveData<List<Employee>>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = Employee::class)
    suspend fun createEmployee(employee: Employee)

    @Query("DELETE from ${ENTITIES.EMPLOYEE} where id=:id")
    suspend fun deleteEmployee(id: Int): Int

    @Query("SELECT * from ${ENTITIES.EMPLOYEE} where id=:id")
    fun getEmployeeForId(id: Int): LiveData<Employee?>

    @Query("SELECT * from ${ENTITIES.CATEGORY}")
    fun getAllCategories(): LiveData<List<Category>>

    @Query("SELECT * from ${ENTITIES.ROLE}")
    fun getAllRoles(): LiveData<List<Role>>

    @Insert(onConflict = OnConflictStrategy.IGNORE, entity = Category::class)
    suspend fun createCategories(categoryList: List<Category>)

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = Category::class)
    suspend fun createCategory(categoryList: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = Role::class)
    suspend fun createRoles(role: List<Role>)

}