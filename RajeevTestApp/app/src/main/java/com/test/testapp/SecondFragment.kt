package com.test.testapp

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.test.testapp.database.entities.Category
import com.test.testapp.database.entities.Employee
import com.test.testapp.database.entities.Role
import com.test.testapp.presenter.EmployeeFormPresenter

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private val presenter = EmployeeFormPresenter()
    lateinit var spinner: Spinner
    lateinit var saveButton: Button
    lateinit var rolesButton: Button
    lateinit var firstName: EditText
    lateinit var lastName: EditText
    lateinit var userName: EditText
    lateinit var email: EditText
    lateinit var password: EditText
    lateinit var confirmPassword: EditText

    private var roles : List<Role>? = null
    private var employee : Employee? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_second, container, false)
        spinner = view.findViewById<Spinner>(R.id.spn_category)
        saveButton = view.findViewById<Button>(R.id.btn_save)
        rolesButton = view.findViewById<Button>(R.id.btn_roles)
        firstName = view.findViewById<EditText>(R.id.edt_firstName)
        lastName = view.findViewById<EditText>(R.id.edt_lastName)
        email = view.findViewById<EditText>(R.id.edt_email)
        userName = view.findViewById<EditText>(R.id.edt_userName)
        password = view.findViewById<EditText>(R.id.edt_password)
        confirmPassword = view.findViewById<EditText>(R.id.edt_cnfrm_password)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.getCategories().observe(viewLifecycleOwner, Observer { categoryList ->
            context?.let {
                spinner.adapter = ArrayAdapter(
                    it,
                    R.layout.category_item,
                    R.id.tv_category,
                    categoryList.toMutableList()
                )
            }
        })

        arguments?.let {
            val employeeId = it.getInt("id")
            if (null != employeeId) {
                presenter.getEmployeeData(employeeId).observe(viewLifecycleOwner, Observer {employee ->
                    if(null != employee) {
                        this.employee = employee
                        firstName.setText(employee.firstName)
                        lastName.setText(employee.lastName)
                        email.setText(employee.emailId)
                        userName.setText(employee.userName)
                        password.setText(employee.password)
                    }
                })
            }
        }

        rolesButton.setOnClickListener {
            presenter.getRoles().observe(viewLifecycleOwner, Observer { roles ->
                this.roles = roles
                context?.let {
                    val builder = AlertDialog.Builder(it).setMultiChoiceItems(
                        getRolesList(roles).toTypedArray(),
                        getSelectedRolesList(roles).toBooleanArray()
                    ) { p0, which, isChecked ->
                        employee?.let {
                            if(isChecked && !it.rolesList.contains(roles[which].id)) {
                                it.rolesList.add(roles[which].id)
                            } else {
                                it.rolesList.remove(roles[which].id)
                            }
                        }
                    }
                    builder.setPositiveButton("OK", { dialogInterface, i ->

                    })
                    builder.setNegativeButton("CANCEL", { dialogInterface, i ->

                    })
                    builder.create().show()
                }
            })

        }

        saveButton.setOnClickListener {
            val employee = Employee()
            employee.firstName = firstName.text.toString()
            employee.lastName = lastName.text.toString()
            employee.emailId = email.text.toString()
            employee.userName = userName.text.toString()
            employee.password = password.text.toString()
            employee.categoryId = spinner.selectedItem.toString()
            presenter.createEmployeeRecord(employee)
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
    }

    private fun getRolesList(roles: List<Role>): List<String> {
        val list = ArrayList<String>(roles.size)
        roles.forEach {
            list.add(it.type)
        }
        return list
    }

    private fun getSelectedRolesList(roles: List<Role>): List<Boolean> {
        val list = ArrayList<Boolean>(roles.size)
        roles.forEach {
            list.add(false)
        }
        return list
    }

}