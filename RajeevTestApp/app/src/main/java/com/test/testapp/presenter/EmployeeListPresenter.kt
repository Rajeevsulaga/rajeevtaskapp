package com.test.testapp.presenter

import androidx.lifecycle.LiveData
import com.test.testapp.database.entities.Employee
import com.test.testapp.repository.EmployeeRepository
import com.test.testapp.view.IEmployeeListView

/**
 * Created by rajeev on 22,August,2020
 */
class EmployeeListPresenter : BasePresenter<IEmployeeListView>() {

    private val repository = EmployeeRepository.INSTANCE

    fun  loadEmployees() : LiveData<List<Employee>>{
        return repository.getAllEmployees()
    }

    fun deleteEmployeeEntry(employee: Employee) {
        repository.deleteEmployeeRecord(employee)
    }

}