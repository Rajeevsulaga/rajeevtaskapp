package com.test.testapp.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.test.testapp.database.ENTITIES

/**
 * Created by rajeev on 22,August,2020
 */
@Entity(
    tableName = ENTITIES.ROLE, indices = [
        Index(name = "roleId", value = ["id"], unique = true)
    ]
)
data class Role(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "type")
    var type: String = ""
) {
    override fun toString(): String {
        return return type ?: ""
    }

    override fun equals(other: Any?): Boolean {
        if(null != other && other is Role) {
            return other.id == id
        }
        return false
    }
}