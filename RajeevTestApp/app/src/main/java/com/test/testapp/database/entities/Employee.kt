package com.test.testapp.database.entities

import androidx.room.*
import com.test.testapp.database.DataConverter
import com.test.testapp.database.ENTITIES
import com.test.testapp.util.Constants
import java.io.Serializable

/**
 * Created by rajeev on 22,August,2020
 */

@Entity(
    tableName = ENTITIES.EMPLOYEE, indices = [
        Index(name = "idIndex", value = ["id"], unique = true)
    ]
)
class Employee(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0,

    @ColumnInfo(name = "first_name")
    var firstName: String? = null,

    @ColumnInfo(name = "last_name")
    var lastName: String? = null,

    @ColumnInfo(name = "user_name")
    var userName: String? = null,

    @ColumnInfo(name = "password")
    var password: String? = null,

    @ColumnInfo(name = "email")
    var emailId: String? = null,

    @ColumnInfo(name = "category_id")
    var categoryId: String? = null

) : Serializable {


    @ColumnInfo(name = "roles")
    @TypeConverters(DataConverter::class)
    var rolesList: MutableList<Int> = ArrayList()

}