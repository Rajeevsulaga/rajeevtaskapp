package com.test.testapp

import android.app.Application
import com.test.testapp.database.DBUtils

class MainApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        DBUtils.onCreate(this)
    }


}