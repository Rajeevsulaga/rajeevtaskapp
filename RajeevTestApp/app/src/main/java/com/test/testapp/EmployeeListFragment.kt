package com.test.testapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.test.testapp.adapter.EmployeeListAdapter
import com.test.testapp.presenter.EmployeeListPresenter

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class EmployeeListFragment : Fragment(), EmployeeListAdapter.OnEmployeeItemClick {

    private val presenter = EmployeeListPresenter()
    private var adapter = EmployeeListAdapter(ArrayList(), this)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val rlvEmployee = view.findViewById<RecyclerView>(R.id.rlv_employee)
        rlvEmployee.apply {
            setHasFixedSize(true)
            adapter = this@EmployeeListFragment.adapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
        initSwipeToDelete(rlvEmployee)

        view.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
           launchEmpDetailScreen()
        }

        presenter.loadEmployees().observe(viewLifecycleOwner, Observer {
            adapter.updateData(it)
        })
    }


    private fun initSwipeToDelete(rlvEmployee : RecyclerView) {
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val employee = adapter.getUserList()[viewHolder.adapterPosition]
                presenter.deleteEmployeeEntry(employee)
            }
        }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(rlvEmployee)
    }

    private fun launchEmpDetailScreen() {
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
    }

    override fun onEmployeeSelected(employeeId: Int) {
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, Bundle().apply { putInt("id", employeeId) })
    }

}