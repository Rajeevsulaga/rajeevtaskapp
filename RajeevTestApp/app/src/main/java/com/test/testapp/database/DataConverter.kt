package com.test.testapp.database

import androidx.room.TypeConverter
import androidx.room.TypeConverters

/**
 * Created by rajeev on 24,August,2020
 */
@TypeConverters
class DataConverter {
    @TypeConverter
    fun toRolesList(csv: String): List<Int> {
        val list = ArrayList<Int>()
        if(csv.isNullOrEmpty()){
            return list
        }
        val stringList = csv.split(",")
        stringList.forEach {
            list.add(it.toInt())
        }
        return list
    }

    @TypeConverter
    fun fromRolesList(roleIds : List<Int>) : String {
        var roleString : StringBuilder = StringBuilder()
        roleIds.forEach {
            roleString.append(it).append(',')
        }
        val finalString = roleString.toString()
        if(finalString.isNotEmpty()) {
            return roleString.toString().substring(0, finalString.length - 2)
        }
        return ""
    }
}