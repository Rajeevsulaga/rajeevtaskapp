package com.test.testapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.test.testapp.R
import com.test.testapp.database.entities.Employee

/**
 * Created by rajeev on 23,August,2020
 */
class EmployeeListAdapter(private var userList: List<Employee>,private val listener: OnEmployeeItemClick) : RecyclerView.Adapter<EmployeeListAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.employee_item, parent, false)
        return ViewHolder(v, listener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: EmployeeListAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    fun updateData(userList: List<Employee>) {
        this.userList = userList
        notifyDataSetChanged()
    }

    fun getUserList() : List<Employee> {
        return userList
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View, private val listener: OnEmployeeItemClick) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(employee: Employee) {
            val textViewName = itemView.findViewById(R.id.tv_name) as TextView
            itemView.setOnClickListener{
                listener.onEmployeeSelected(employee.id)
            }
            textViewName.text = employee.firstName + " " + employee.lastName
        }
    }

    open interface OnEmployeeItemClick {
        fun onEmployeeSelected(position :Int)
    }
}