package com.test.testapp.presenter

import androidx.lifecycle.LiveData
import com.test.testapp.database.entities.Role
import com.test.testapp.repository.EmployeeRepository
import com.test.testapp.view.IEmployeeRolesView

/**
 * Created by rajeev on 24,August,2020
 */
class EmployeeRolesPresenter : BasePresenter<IEmployeeRolesView>() {
    private val repository = EmployeeRepository.INSTANCE

    fun getRoles() : LiveData<List<Role>> {
        return repository.getEmployeeRoles()
    }

}