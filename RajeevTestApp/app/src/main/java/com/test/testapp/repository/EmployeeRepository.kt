package com.test.testapp.repository

import android.os.Handler
import androidx.lifecycle.LiveData
import com.test.testapp.database.DBUtils
import com.test.testapp.database.entities.Category
import com.test.testapp.database.entities.Employee
import com.test.testapp.database.entities.Role
import kotlinx.coroutines.runBlocking

/**
 * Created by rajeev on 22,August,2020
 */
class EmployeeRepository private constructor() {
    private var employeeLiveData: LiveData<List<Employee>>? = null
    private var categoryLiveData: LiveData<List<Category>>? = null
    private var rolesLiveData: LiveData<List<Role>>? = null
    private var dbThreadHandler : Handler = DBUtils.getDatabase().dbThreadHandler

    companion object {
        val INSTANCE: EmployeeRepository by lazy { EmployeeRepository() }
    }

    fun getAllEmployees(): LiveData<List<Employee>> {
        if (null == employeeLiveData) {
            employeeLiveData = DBUtils.getAllEmployees();
        }
        return employeeLiveData!!
    }

    fun getEmployeeCategories(): LiveData<List<Category>> {
        if (null == categoryLiveData) {
            categoryLiveData = DBUtils.getEmployeeCategories()
        }
        return categoryLiveData!!
    }

    fun getEmployeeRoles(): LiveData<List<Role>> {
        if (null == rolesLiveData) {
            rolesLiveData = DBUtils.getEmployeeRoles()
        }
        return rolesLiveData!!
    }

    fun deleteEmployeeRecord(employee: Employee) {
        dbThreadHandler.post {
            runBlocking {
                DBUtils.deleteEmployeeRecord(employee.id)
            }
        }
    }

    fun createEmployee(employee: Employee) {
        dbThreadHandler.post {
            runBlocking {
                DBUtils.createEmployee(employee)
            }
        }
    }

    fun getEmployeeForId(id: Int): LiveData<Employee?> {
        return DBUtils.getEmployeeData(id)
    }

}