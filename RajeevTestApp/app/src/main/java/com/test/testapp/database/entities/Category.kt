package com.test.testapp.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.test.testapp.database.ENTITIES

/**
 * Created by rajeev on 22,August,2020
 */
@Entity(
    tableName = ENTITIES.CATEGORY, indices = [
        Index(name = "cat", value = ["type"], unique = true)
    ]
)
data class Category(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "type")
    val type: String = ""
) {

    override fun toString(): String {
        return return type ?: ""
    }
}